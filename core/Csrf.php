<?php

// Copyright (c) 2020 CYBUTEK Solutions. All rights reserved.
// File: Csrf.php

class Csrf
{
    public static function generate($form_id): string
    {
        $form_id = base64_encode($form_id);
        $token =  base64_encode(random_bytes(32));
        $_SESSION['csrf'][$form_id] = $token;
        return '<input name="csrf:' . $form_id . '" type="hidden" value="' . $token . '" />';
    }

    public static function validate($form_id): bool
    {
        $form_id = base64_encode($form_id);
        $session_token = isset($_SESSION['csrf'][$form_id]) ? $_SESSION['csrf'][$form_id] : null;
        $form_token = isset($_REQUEST['csrf:' . $form_id]) ? $_REQUEST['csrf:' . $form_id] : null;
        return $session_token && $form_token && $session_token == $form_token;
    }
}