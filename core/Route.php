<?php

// Copyright (c) 2020 CYBUTEK Solutions. All rights reserved.
// File: Route.php

class Route
{
    private string $url;
    private int $url_length;
    private $action;

    public function __construct($url, $action)
    {
        $this->url = trim($url, '/');
        $this->url_length = strlen($url);
        $this->action = $action;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getUrlLength(): int
    {
        return $this->url_length;
    }

    public function getOutput(array $params): ?string
    {
        global $app_dir;

        if (is_string($this->action)) {
            $filepath  = $app_dir . '/routes/' . trim($this->action, '/') . '.php';

            if (file_exists($filepath)) {
                ob_start();
                include $filepath;
                return ob_get_clean();
            }
        }

        if (is_callable($this->action)) {
            return call_user_func($this->action);
        }

        return null;
    }

    public function compare($url): int
    {
        $compare_url_length = strlen($url);

        if ($this->url_length <= $compare_url_length) {
            if (substr($url, 0, $this->url_length) == $this->url) {
                return $this->url_length;
            }
        }

        return 0;
    }
}