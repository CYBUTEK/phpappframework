<?php

// Copyright (c) 2020 CYBUTEK Solutions. All rights reserved.
// File: Router.php

class Router
{
    private static array $routes = array();
    private static string $prefix;

    public static function set(Route $route)
    {
        array_push(self::$routes, $route);
    }

    public static function get(string $url = null): ?string
    {
        if ($url == null) {
            if (isset($_GET['route'])) {
                $url = $_GET['route'];

                if (isset(self::$prefix)) {
                    $url = str_replace(self::$prefix, '', $url);
                }
            } else {
                $url = '';
            }
        }

        $url = trim($url, '/');
        $route = self::findBestMatch($url);
        $params = explode('/', trim(substr($url, strlen($route->getUrl())), '/'));

        return $route->getOutput($params);
    }

    public static function link(string $url): string
    {
        if (isset(self::$prefix)) {
            $url = trim(self::$prefix, '/') . '/' . trim($url, '/');
        }

        return '/' . trim($url, '/');
    }

    public static function prefix(string $prefix)
    {
        self::$prefix = trim($prefix, '/');
    }

    private static function findBestMatch($url): Route
    {
        global $app_dir;

        if (file_exists($app_dir . '/routes/' . $url . '.php')) {
            return new Route($url, $url);
        }

        $selected_url_length = 0;
        $selected_route = null;

        foreach (self::$routes as $route) {
            if ($route->getUrl() == $url) {
                return $route;
            }

            $compare_length = $route->compare($url);

            if ($compare_length >= $selected_url_length) {
                $selected_url_length = $compare_length;
                $selected_route = $route;
            }
        }

        return $selected_route;
    }
}