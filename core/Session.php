<?php

// Copyright (c) 2020 CYBUTEK Solutions. All rights reserved.
// File: Session.php

session_start();

class Session
{
    public static function get(string $key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }

        return null;
    }

    public static function set(string $key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function unset(string $key)
    {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }
}