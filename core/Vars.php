<?php

// Copyright (c) 2020 CYBUTEK Solutions. All rights reserved.
// File: Vars.php

class Vars
{
    private static array $storage = array();

    public static function get(string $key)
    {
        if (array_key_exists($key, self::$storage)) {
            return self::$storage[$key];
        }
    }

    public static function set(string $key, $value)
    {
        self::$storage[$key] = $value;
    }
}