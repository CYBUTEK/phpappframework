<?php

// Copyright (c) 2020 CYBUTEK Solutions. All rights reserved.
// File: View.php

class View
{
    public static function get($name): ?string
    {
        global $app_dir;

        $filepath = $app_dir . '/views/' . $name . '.php';

        if (file_exists($filepath)) {
            ob_start();
            include $filepath;
            return ob_get_clean();
        }

        return null;
    }
}