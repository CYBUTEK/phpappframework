<?php

// Load framework implementation
foreach (glob(__DIR__ . '/core/*.php') as $filename) {
    include $filename;
}

// Load framework configuration
foreach (glob($app_dir . '/config/*.php') as $filename) {
    include $filename;
}