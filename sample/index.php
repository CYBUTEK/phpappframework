<?php

$app_dir = __DIR__ . '/app';

include './framework/main.php';
$page_body = Router::get();
$page_title = Vars::get('page_title');

?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?= $page_title ? $page_title . ' - ' : null ?>Example Website</title>
        <link href="/css/styles.min.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <?= $page_body ?>
    </body>

</html>